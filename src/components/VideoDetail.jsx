import React, {useEffect, useState} from 'react'
import {Box, Stack, Typography} from "@mui/material";
import ReactPlayer from "react-player";
import {useParams} from "react-router-dom";
import {fetchFromAPI} from "../utils/FetchFromAPI";

const VideoDetail = () => {
    const [videoDetail, setVideoDetail] = useState(null);
    const {id} = useParams();

    useEffect(() => {
        fetchFromAPI(`videos?part=snippet,statistics&id=${id}`).then((data) => setVideoDetail(data.items[0]));
    }, [id]);


    return(
        <Box minHeight="95vh">
            <Stack direction={{xs:'column', md:'row'}}>
                <Box flex={1}>
                    <Box sx={{width: '100%', position: 'sticky', top: '86px'}}>
                        <ReactPlayer url={`https://www.youtube.com/watch?v=${id}`} className="react-player" controls />
                        <Typography color="#fff" variant="h5" fontWeight="bold" p={2}>
                            {videoDetail?.snippet?.title}
                        </Typography>
                    </Box>
                </Box>
            </Stack>
        </Box>
    )
}

export default VideoDetail
